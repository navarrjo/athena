# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

## Monitoring triggers
## Modify to the default naming scheme for menu-aware montioring.

monitoring_l1jet = [ 'L1_J10',
                     'L1_TE50',
                     'L1_J15',
                     'L1_J75',
                     'L1_J100',
                     'L1_J100.31ETA49',
                     'L1_J400']

monitoring_jet = ['j35',
                  'j35_jes',
                  'j35_lcw',
                  'j35_sub',
                  'j35_subjes',
                  'j35_nojcalib',
                  'j35_320eta490',
                  'j35_320eta490_lcw',
                  'j35_320eta490_jes',
                  'j35_320eta490_sub',
                  'j35_320eta490_subjes',
                  'j35_320eta490_nojcalib',
                  'j60',
                  'j60_280eta320',
                  'j60_320eta490',
                  'j60_L1RD0_FILLED',
                  'j260',
                  'j360',
                  'j420',
                  'j225_gsc420_boffperf_split',
                  'j460_a10_lcw_subjes_L1SC111',
                  'j460_a10t_lcw_jes_L1SC111',
                  '2j330_a10t_lcw_jes_35smcINF_SC111',
                  'j80_xe80',
                  '4j45',
                  '6j45',
                  '6j45_0eta240',
                  '10j40_L14J20']

primary_l1jet = ['L1_J10',
                 'L1_TE50',
                 'L1_J15',
                 'L1_J75',
                 'L1_J100',
                 'L1_J100.31ETA49',
                 'L1_J400']

primary_jet = ['j35',
               'j35_jes',
               'j35_lcw',
               'j35_sub',
               'j35_subjes',
               'j35_nojcalib',
               'j35_320eta490',
               'j35_320eta490_lcw',
               'j35_320eta490_jes',
               'j35_320eta490_sub',
               'j35_320eta490_subjes',
               'j35_320eta490_nojcalib',
               'j60',
               'j60_280eta320',
               'j60_320eta490',
               'j60_L1RD0_FILLED',
               'j260',
               'j360',
               'j420',
               'j225_gsc420_boffperf_split',
               'j460_a10_lcw_subjes_L1SC111',
               'j460_a10t_lcw_jes_L1SC111',
               '2j330_a10t_lcw_jes_35smcINF_SC111',
               'j80_xe80',
               '4j45',
               '6j45',
               '6j45_0eta240',
               '10j40_L14J20']

########################## pp Config ###############################################

monitoring_l1jet_pp = [ 'L1_J10',
                        'L1_TE50',
                        'L1_J15',
                        'L1_J75',
                        'L1_J100',
                        'L1_J100.31ETA49',
                        'L1_J400']


monitoring_jet_pp = ['j35',
                     'j35_jes',
                     'j35_lcw',
                     'j35_sub',
                     'j35_subjes',
                     'j35_nojcalib',
                     'j35_320eta490',
                     'j35_320eta490_lcw',
                     'j35_320eta490_jes',
                     'j35_320eta490_sub',
                     'j35_320eta490_subjes',
                     'j35_320eta490_nojcalib',
                     'j60',
                     'j60_280eta320',
                     'j60_320eta490',
                     'j60_L1RD0_FILLED',
                     'j260',
                     'j360',
                     'j420',
                     'j225_gsc420_boffperf_split',
                     'j460_a10_lcw_subjes_L1SC111',
                     'j460_a10t_lcw_jes_L1SC111',
                     '2j330_a10t_lcw_jes_35smcINF_SC111',
                     'j80_xe80',
                     '4j45',
                     '6j45',
                     '6j45_0eta240',
                     '10j40_L14J20']

primary_l1jet_pp = ['L1_J10',
                    'L1_TE50',
                    'L1_J15',
                    'L1_J75',
                    'L1_J100',
                    'L1_J100.31ETA49',
                    'L1_J400']

primary_jet_pp = ['j35',
                  'j35_jes',
                  'j35_lcw',
                  'j35_sub',
                  'j35_subjes',
                  'j35_nojcalib',
                  'j35_320eta490',
                  'j35_320eta490_lcw',
                  'j35_320eta490_jes',
                  'j35_320eta490_sub',
                  'j35_320eta490_subjes',
                  'j35_320eta490_nojcalib',
                  'j60',
                  'j60_280eta320',
                  'j60_320eta490',
                  'j60_L1RD0_FILLED',
                  'j260',
                  'j360',
                  'j420',
                  'j225_gsc420_boffperf_split',
                  'j460_a10_lcw_subjes_L1SC111',
                  'j460_a10t_lcw_jes_L1SC111',
                  '2j330_a10t_lcw_jes_35smcINF_SC111',
                  'j80_xe80',
                  '4j45',
                  '6j45',
                  '6j45_0eta240',
                  '10j40_L14J20']

########################## validation Config ###############################################


monitoring_l1jet_validation = [ 'L1_J10',
                                'L1_TE50',
                                'L1_J15',
                                'L1_J75',
                                'L1_J100',
                                'L1_J100.31ETA49',
                                'L1_J400']


monitoring_jet_validation = ['j35',
                             'j35_jes',
                             'j35_lcw',
                             'j35_sub',
                             'j35_subjes',
                             'j35_nojcalib',
                             'j35_320eta490',
                             'j35_320eta490_lcw',
                             'j35_320eta490_jes',
                             'j35_320eta490_sub',
                             'j35_320eta490_subjes',
                             'j35_320eta490_nojcalib',
                             'j60',
                             'j60_280eta320',
                             'j60_320eta490',
                             'j60_L1RD0_FILLED',
                             'j260',
                             'j360',
                             'j420',
                             'j225_gsc420_boffperf_split',
                             'j460_a10_lcw_subjes_L1SC111',
                             'j460_a10t_lcw_jes_L1SC111',
                             '2j330_a10t_lcw_jes_35smcINF_SC111',
                             'j80_xe80',
                             '4j45',
                             '6j45',
                             '6j45_0eta240',
                             '10j40_L14J20']

primary_l1jet_validation = ['L1_J10',
                            'L1_TE50',
                            'L1_J15',
                            'L1_J75',
                            'L1_J100',
                            'L1_J100.31ETA49',
                            'L1_J400']


primary_jet_validation = ['j35',
                          'j35_jes',
                          'j35_lcw',
                          'j35_sub',
                          'j35_subjes',
                          'j35_nojcalib',
                          'j35_320eta490',
                          'j35_320eta490_lcw',
                          'j35_320eta490_jes',
                          'j35_320eta490_sub',
                          'j35_320eta490_subjes',
                          'j35_320eta490_nojcalib',
                          'j60',
                          'j60_280eta320',
                          'j60_320eta490',
                          'j60_L1RD0_FILLED',
                          'j260',
                          'j360',
                          'j420',
                          'j225_gsc420_boffperf_split',
                          'j460_a10_lcw_subjes_L1SC111',
                          'j460_a10t_lcw_jes_L1SC111',
                          '2j330_a10t_lcw_jes_35smcINF_SC111',
                          'j80_xe80',
                          '4j45',
                          '6j45',
                          '6j45_0eta240',
                          '10j40_L14J20']

######################## Cosmics Config ############################################

monitoring_l1jet_cosmic = ['L1_J10',
                           'L1_J15']

monitoring_jet_cosmic = ['j25']

primary_l1jet_cosmic = ['L1_J10',
                        'L1_J15']

primary_jet_cosmic = ['j25']



###################################### HI Config #######################################

monitoring_l1jet_hi = ['L1_J20',
                       'L1_J30',
                       'L1_TE50',
                       'L1_J15.31ETA49',
                       'L1_J20.31ETA49']
                       
monitoring_jet_hi = ['j85_ion_TE50',
                     'j85_ion_L1J20',
                     'j100_ion_L1J30',
                     'j110_ion_L1J30',
                     'j120_ion_L1J30',
                     'j50_320eta490_ion',
                     'j60_320eta490_ion',
                     'j70_320eta490_ion',
                     'mu4_j30_a2_ion_dr05',
                     'mu4_j40_a2_ion_dr05',
                     'mu4_j30_a2_ion',
                     'mu4_j40_a2_ion']

primary_l1jet_hi = ['L1_J20',
                    'L1_J30',
                    'L1_TE50',
                    'L1_J15.31ETA49',
                    'L1_J20.31ETA49']
                     
primary_jet_hi = ['j85_ion_TE50',
                  'j85_ion_L1J20',
                  'j100_ion_L1J30',
                  'j110_ion_L1J30',
                  'j120_ion_L1J30',
                  'j50_320eta490_ion',
                  'j60_320eta490_ion',
                  'j70_320eta490_ion',
                  'mu4_j30_a2_ion_dr05',
                  'mu4_j40_a2_ion_dr05',
                  'mu4_j30_a2_ion',
                  'mu4_j40_a2_ion']
